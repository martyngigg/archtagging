/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 *all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_PANDA_RESOURCEPOOLIMPL_H
#define SKA_PANDA_RESOURCEPOOLIMPL_H

#include "panda/Resource.h"

#include <deque>
#include <iostream>
#include <memory>

namespace ska {
namespace panda {
namespace detail {

template <class Arch> struct TaskHolder {

  void operator()(PoolResource<Arch> &r) {
    std::cout << "Launching task on resource=" << &r << "\n";
  }
};

/**
 * @class ResourcePoolImpl
 *
 * @brief
 *    A Processor Resource Manager and Job Queue
 *
 * @details
 *    Allows you to submit jobs to a queue of Compute resources
 *    As the resource becomes available, the next item from the queue
 *    is taken and executed.
 *
 *    Use the @code add_resource() method to add Resources to be managed. Note
 *that
 *    all the resources should be compatible with the types of jobs to be
 *    submitted - there is no checking of job suitability.
 *
 *    call @code submit() to add a job to be processed. All job status
 *    information/callbacks etc can be found through the ProcessorJob interface.
 *
 *    @tparam the number of priority queues to assign, 1 = a single queue with
 *eqaul priority to all tasks
 */
template <unsigned MaxPriority>
class ResourcePoolImpl : public std::enable_shared_from_this<ResourcePoolImpl<MaxPriority>> {
public:
  template<typename... Architectures>
  ResourcePoolImpl(Architectures...) = default;

private:
  // prioritised queues (0 = highest priority)
  template <typename Arch>
  using QueueType = std::array<std::deque<TaskHolder<Arch>>, MaxPriority>;

  std::tuple<QueueType<Architectures>...> _queue;
};

} // namespace detail
} // namespace panda
} // namespace ska

#endif // SKA_PANDA_RESOURCEPOOLIMPL_H
