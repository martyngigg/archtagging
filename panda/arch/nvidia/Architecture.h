/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 *all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_PANDA_NVIDIA_NVIDIA_ARCHITECTURE_H
#define SKA_PANDA_NVIDIA_NVIDIA_ARCHITECTURE_H

#include "panda/Architecture.h"
#include <boost/mpl/comparison.hpp>
#include <boost/mpl/find_if.hpp>
#include <boost/mpl/vector.hpp>

#include <type_traits>

namespace ska {
namespace panda {
namespace nvidia {

/**
 * @brief Architecture name tag for Cuda enabled NVidia devices
 */
class Cuda : public panda::Architecture {
public:
  Cuda();
};

/**
 * @brief Defines a specific Cuda architecture based on its compute
 * capability version
 * @tparam Major The major version of the capability version
 * @tparam Minor The minor version of the capability version
 */
template <int Major, int Minor> struct CudaVersion : Cuda {
  static_assert(Major >= 1, "Major is required >= 1");
  static_assert(Minor >= 0, "Minor is required >= 0");

  static constexpr int major_version = Major;
  static constexpr int minor_version = Minor;
};

/**
 * @brief General implementation for checking a single
 * Cuda architecture version is compatible. Always returns false
 */
  template <class T, class U, class Enable = void>
struct IsCompatibleImpl : std::false_type {};

/**
 * @brief Specialisation for Cuda to always match
 */
template <class T> struct IsCompatibleImpl<Cuda, T> : std::true_type {};

/**
 * Is V1 compatible with V2
 */
template <class V1, class V2> struct _IsCompatibleHelper {
  typedef boost::mpl::int_<V1::major_version> _v1_major;
  typedef boost::mpl::int_<V1::minor_version> _v1_minor;
  typedef boost::mpl::int_<V2::major_version> _v2_major;
  typedef boost::mpl::int_<V2::minor_version> _v2_minor;

  static const bool _value =
      (boost::mpl::less<_v1_major, _v2_major>::value ||
       (boost::mpl::equal_to<_v1_major, _v2_major>::value &&
        boost::mpl::less_equal<_v1_minor, _v2_minor>::value));
};

/**
 * @brief Specialisation for matching CudaVersions
 */
template <class T, int Major, int Minor>
struct IsCompatibleImpl<T, CudaVersion<Major, Minor>,
                        typename std::enable_if<!std::is_same<T, Cuda>::value>::type>
    : std::integral_constant<
          bool, _IsCompatibleHelper<T, CudaVersion<Major, Minor>>::_value> {};


} // namespace nvidia

/**
 * @brief Specialisation of IsCompatible for Cuda devices to perform a
 * more detailed check.
 * @details It is assumed that any given cuda version is backwards compatible
 * with any previous version. Returns true any version in the Architectures
 * tuple
 * is >= to the required architecture.
 */
template <class Arch, class A1, class... An>
struct IsCompatible<
    Arch, std::tuple<A1, An...>,
    typename std::enable_if<std::is_base_of<nvidia::Cuda, Arch>::value>::type> {
  typedef boost::mpl::vector<A1, An...> arch_list;
  typedef typename boost::mpl::find_if<
      arch_list, typename boost::mpl::lambda<nvidia::IsCompatibleImpl<
                     Arch, boost::mpl::placeholders::_1>>::type>::type
      arch_list_iter;

  static constexpr bool value =
      !boost::is_same<arch_list_iter,
                      typename boost::mpl::end<arch_list>::type>::value;
};

} // namespace panda
} // namespace ska

#endif // SKA_PANDA_NVIDIA_NVIDIA_ARCHITECTURE_H
