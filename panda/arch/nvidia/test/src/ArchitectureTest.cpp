/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "ArchitectureTest.h"
#include "panda/arch/nvidia/Architecture.h"


namespace ska {
namespace panda {
namespace nvidia{
namespace test {


ArchitectureTest::ArchitectureTest()
    : ::testing::Test()
{
}

ArchitectureTest::~ArchitectureTest()
{
}

void ArchitectureTest::SetUp()
{
}

void ArchitectureTest::TearDown()
{
}

struct NotCuda : Architecture {};

TEST_F(ArchitectureTest, test_ctor)
{
    Cuda arch;
    ASSERT_EQ(arch.tag(), "nvidia::Cuda");
}

TEST_F(ArchitectureTest, test_is_compatible)
{
  typedef std::tuple<CudaVersion<3,5>, CudaVersion<2,1>> Architectures;
   ASSERT_FALSE((IsCompatible<NotCuda, Architectures>::value));
  // General Cuda matches
  ASSERT_TRUE((IsCompatible<Cuda, Architectures>::value));

  // Version matches
  ASSERT_FALSE((IsCompatible<CudaVersion<4, 5>, Architectures>::value));
  ASSERT_FALSE((IsCompatible<CudaVersion<3, 6>, Architectures>::value));
  ASSERT_TRUE((IsCompatible<CudaVersion<3, 5>, Architectures>::value));
  ASSERT_TRUE((IsCompatible<CudaVersion<2, 1>, Architectures>::value));
  ASSERT_TRUE((IsCompatible<CudaVersion<1, 3>, Architectures>::value));

}


} // namespace test
} // namespace nvidia
} // namespace panda
} // namespace ska
