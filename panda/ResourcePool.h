/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_PANDA_RESOURCEPOOL_H
#define SKA_PANDA_RESOURCEPOOL_H

#include "panda/detail/ResourcePoolImpl.h"
#include <memory>

namespace ska {
namespace panda {
class Resource;
class ProcessingEngine;




//template<typename ResourcePoolTraits>
/**
 *  \ingroup resource
 *  @{
 *
 * @brief
 *    Match well defined Resources in a pool to Tasks submitted
 * @details
 * @tparam MaxPriority the number of priority levels that are to be supported (1 = all tasks equal priority)
 *
 * Note that copies are allowed. Each will refer to the same underlying pool of resources. This allows setting
 * of different priority offsets etc.
 *
 */
template<unsigned MaxPriority>
class ResourcePool
{
    public:
        ResourcePool();
        ~ResourcePool();

       /// @brief add a Resource (e.g. an NVidia card, or a Processor Core) to manage
       //  @param Arch - specify the type of resource the passed object is to be interpreted as
      template<typename... Architectures>
      void set_resources(Architectures... archs);

    private:
        std::shared_ptr<detail::ResourcePoolImpl<MaxPriority>> _pimpl;
        unsigned _priority;
};


} // namespace panda
} // namespace ska
#include "panda/detail/ResourcePool.cpp"

#endif // SKA_PANDA_RESOURCEPOOL_H
/**
 *  @}
 *  End Document Group
 **/
