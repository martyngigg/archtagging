/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_PANDA_TUPLEUTILITIES_H
#define SKA_PANDA_TUPLEUTILITIES_H

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wall"
#pragma GCC diagnostic ignored "-Wpragmas"
#pragma GCC diagnostic ignored "-Wunused-parameter"
#pragma GCC diagnostic ignored "-Wunused-variable"
#pragma GCC diagnostic ignored "-Wnon-template-friend"
#include <boost/mpl/placeholders.hpp>
#include <boost/mpl/sort.hpp>
#include <boost/mpl/list.hpp>
#pragma GCC diagnostic pop
#include <tuple>

namespace ska {
namespace panda {

/**
 * @brief A collection of utilities to manipulate the std::tuple
 */

///--------------------------------------------------------------------
// @brief Index
// @details Static helper function template to return the index of a
//          given type in a tuple.
// @member value : the index of the type
// --------------------------------------------------------------------
template <class T, class Tuple>
struct Index;

template <class T, class... Types>
struct Index<T, std::tuple<T, Types...>> {
    static const std::size_t value = 0;
};

template <class T, class U, class... Types>
struct Index<T, std::tuple<U, Types...>> {
    static const std::size_t value = 1 + Index<T, std::tuple<Types...>>::value;
};

///--------------------------------------------------------------------
// @brief HasType
// @details Static helper function template to indicate wheter a
//          given type is present in a tuple.
// @member value : true if the type is in the tuple
// --------------------------------------------------------------------
template <class T, class Tuple>
struct HasType {
    static const bool value = false;
};

template <class T>
struct HasType<T, T> {
    static const bool value = true;
};

template <class T, class... Types>
struct HasType<T, std::tuple<T, Types...>> {
    static const bool value = true;
};

template <class T, class U, class... Types>
struct HasType<T, std::tuple<U, Types...>> {
    static const bool value = HasType<T, std::tuple<Types...>>::value;
};

template <typename T, template <typename...> class Type, typename... TTypes>
struct HasType<T, Type<T, TTypes...>> {
    static const bool value = true;
};

template <typename T, template <typename...> class Type, typename U, typename... TTypes>
struct HasType<T, Type<U, TTypes...>> {
    static const bool value = HasType<T, Type<TTypes...>>::value;
};

///--------------------------------------------------------------------
// @brief for_each
// @details iterate over a tuple calling a suitable function f for each Type
//          The FuncType must be a type that defines the operator()(T) for
//          each type.
// --------------------------------------------------------------------

template<std::size_t I, typename FuncType, typename... Tp>
inline typename std::enable_if<I == sizeof...(Tp), void>::type
for_each(std::tuple<Tp...>&, FuncType&&)
{ }

template<std::size_t I, typename FuncType, typename... Tp>
inline typename std::enable_if<I == sizeof...(Tp), void>::type
for_each(std::tuple<Tp...> const&, FuncType&&)
{ }

template<std::size_t I = 0, typename FuncType, typename... Tp>
inline typename std::enable_if<I < sizeof...(Tp), void>::type
for_each(std::tuple<Tp...>& t, FuncType&& f)
{
    typedef std::tuple<Tp...> TupleType;
    f(std::get<I>(t));
    for_each<I + 1, FuncType, Tp...>(std::forward<TupleType&>(t), std::forward<FuncType>(f));
}

template<std::size_t I = 0, typename FuncType, typename... Tp>
inline typename std::enable_if<I < sizeof...(Tp), void>::type
for_each(std::tuple<Tp...> const& t, FuncType&& f)
{
    typedef std::tuple<Tp...> const TupleType;
    f(std::get<I>(t));
    for_each<I + 1, FuncType, Tp...>(std::forward<TupleType const&>(t), std::forward<FuncType>(f));
}

/**
 * @brief iterate over each type in a tuple, without actually having a concrete instance of the tuple
 * @details for each Type will call the passed function template operator<Type>() method
 *
 * @code
 * typedef std::tuple<A, B, C> MyTupleType;
 *
 * temaplte<typename T>
 * struct MyFunctionTemplate {
 *    inline void operator()() {
 *          // do something here T specific
 *    }
 * };
 *
 * panda::ForEach<MyTupleType, MyFunctionTemplate>::exec();
 * @endcode
 *
 * n.b. you can pass in other params through exec to call the corresponding
 * operaotr()(...) on your template.
 */
template<typename TupleType, template <typename> class>
struct ForEach {
    template<typename... DataTypes>
    static inline void exec(DataTypes&&...) {}
};

template<template <typename> class FunctionTemplate, typename T, typename... Tp>
struct ForEach<std::tuple<T, Tp...>, FunctionTemplate> {
    template<typename... DataTypes>
    static inline void exec(DataTypes&&... data) {
        FunctionTemplate<T>()(std::forward<DataTypes>(data)...);
        ForEach<std::tuple<Tp...>, FunctionTemplate>::exec(std::forward<DataTypes>(data)...);
    }
};

///--------------------------------------------------------------------
// @brief delete_tuple
// @details delete all pointers in a tuple
// --------------------------------------------------------------------
template<typename... Types>
struct Deleter {
    template <class T>
    void operator()(T ptr) {
        delete ptr;
    }
};

template<typename... Types>
inline void delete_tuple(std::tuple<Types...>& tp)
{
    panda::for_each(tp, Deleter<Types...>());
}

///--------------------------------------------------------------------
// @brief Transfer the types defined in one template param pack into another template
//
// @code
// typedef typename TypeTransfer<std::tuple<A,B,C>, MyTemplateClass>::type type;
// @endcode
// --------------------------------------------------------------------
template<typename From, template <typename...> class To>
struct TypeTransfer;

template<template <typename...> class From, template <typename...> class To, typename... Tp>
struct TypeTransfer<From<Tp...>, To>
{
    typedef To<Tp...> type;
};

///--------------------------------------------------------------------
// @brief Sort the types of a tuple in order of the types idefined Tag  (will not work unless this is defines)
// @details the types to be ordered must have a numerical Tag defined to describe ordering
// --------------------------------------------------------------------
template<typename>
struct Sort;

template<typename... Types>
struct Sort<std::tuple<Types...>> {
    typedef typename TypeTransfer<typename boost::mpl::sort<boost::mpl::list<Types...>>::type, std::tuple>::type type;
};


///--------------------------------------------------------------------
// @brief create a mixin object
// @details create a mixin given a list of types
//
// @code
// MixinTemplateClassA<MixinTemplateClassB<BaseClass>>>* my_object_ptr = Mixin<BaseClass, MininTemplateClassA, MixinTemplateClassB>;
// @endcode
///--------------------------------------------------------------------

/*
template<int I, typename Base, template<typename> class... Tp>
struct MixinTypeGenerator;

template<int I, typename Base, template<typename> class T, template<typename> class... Tp>
struct MixinTypeGenerator<I, Base, T, Tp...> {
    typedef T<typename MixinTypeGenerator<I - 1, Base, Tp...>::type> type;
};

template<typename Base, template<typename> class... Tp>
struct MixinTypeGenerator<0, Base, Tp...>
{
    typedef Base type;
};

template<typename Base, template<typename> class... Tp>
struct Mixin
{
    typedef typename MixinTypeGenerator<sizeof...(Tp), Base, Tp...>::type type;

    static type* create() {
        return new type;
    }

    template<typename... Args>
    static type* create(Args&&... args) {
        return new type(std::forward<Args>(args)...);
    }
};
*/

///--------------------------------------------------------------------
// @brief create a mixin object by extending a base object
// @details the original object is subsummed by the new object
//          n.b. Extension Type must have a copy constructor that takes the BaseType
///--------------------------------------------------------------------

template<template<typename> class Extension, typename BaseType>
inline Extension<BaseType>* extend(BaseType base)
{
    return new Extension<BaseType>(base);
}

} // namespace panda
} // namespace ska

#endif // SKA_PANDA_TUPLEUTILITIES_H
