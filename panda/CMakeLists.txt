set (LIB_SRCS
  src/Architecture.cpp
)

set (LIB_HEADERS
  Architecture.h
)
set (PANDA_LIBRARIES panda)

# Each arch adds to this list
set (LIB_ARCH_SRCS)
add_subdirectory (arch)

add_library (panda ${LIB_SRCS} ${LIB_ARCH_SRCS} ${LIB_HEADERS})

add_subdirectory (test)
