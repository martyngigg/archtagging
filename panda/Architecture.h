/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_PANDA_ARCHITECTURE_H
#define SKA_PANDA_ARCHITECTURE_H

#include "panda/TupleUtilities.h"
#include <type_traits>
#include <string>

namespace ska {
namespace panda {

/**
 * @brief
 *   Base class for all tags
 *   tag for marking architecture of accelerators
 */
class Architecture
{
    protected:
        Architecture(std::string const& id);

    public:
        std::string const& tag() const;

    private:
        const std::string _tag;
};

class Cpu;

/**
 * @brief static time function to return the Arch associated with any given type
 * @details specialise for any specific types
 * defualt type is Cpu unless otherwise overridden
 */
template<typename T, typename Enable=void>
struct ArchitectureType
{
    typedef Cpu type;
};

/**
 * @brief Compile-time numerical meta-function to return whether an architecture is
 * compatible with the given tuple of architectures
 * @details specialise for any specific types. Default behaviour performs an exact
 * type match in the architectures list.
 * @tparam Arch Type identifying an architecture
 * @tparam ArchitecturesTuple std::tuple<> of architecture types
 * @tparam Enable Allows selection of template specialisations through SFINAE
 */
template <class Arch, class ArchitecturesTuple,
          class Enable = void>
struct IsCompatible {
    static_assert(std::is_base_of<Architecture, Arch>::value,
                "Arch type must derive from Architecture");

    static constexpr bool value = HasType<Arch, ArchitecturesTuple>::value;
};


} // namespace panda
} // namespace ska

#endif // SKA_PANDA_ARCHITECTURE_H
