/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "ResourcePoolImplFactoryTest.h"
#include "panda/ResourcePool.h"

namespace ska {
namespace panda {
namespace test {


ResourcePoolImplFactoryTest::ResourcePoolImplFactoryTest()
    : ::testing::Test()
{
}

ResourcePoolImplFactoryTest::~ResourcePoolImplFactoryTest()
{
}

void ResourcePoolImplFactoryTest::SetUp()
{
}

void ResourcePoolImplFactoryTest::TearDown()
{
}

struct ArchA {};
struct ArchB {};

TEST_F(ResourcePoolImplFactoryTest, test_create)
{
  // Define pool with main architecture
  ResourcePool<0> pool;
  pool.set_resources(ArchA(), ArchB());
}


} // namespace test
} // namespace panda
} // namespace ska
