/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "ArchitectureTest.h"
#include "panda/Architecture.h"


namespace ska {
namespace panda {
namespace test {


ArchitectureTest::ArchitectureTest()
    : ::testing::Test()
{
}

ArchitectureTest::~ArchitectureTest()
{
}

void ArchitectureTest::SetUp()
{
}

void ArchitectureTest::TearDown()
{
}


class MyArch: public Architecture {
public:
    MyArch(std::string const& id):Architecture(id) {}
};

class MyArch2: public Architecture {
public:
  MyArch2(std::string const& id):Architecture(id) {}
};

class NotArch {};


TEST_F(ArchitectureTest, test_ctor)
{
    std::string tag("arch_tag");

    MyArch arch(tag);

    ASSERT_EQ(arch.tag(),tag);
}

TEST_F(ArchitectureTest, test_is_compatible)
{
    // True tests
    ASSERT_TRUE((IsCompatible<MyArch, std::tuple<MyArch>>::value));
    ASSERT_TRUE((IsCompatible<MyArch, std::tuple<MyArch, MyArch2>>::value));

    // False tests
    ASSERT_FALSE((IsCompatible<MyArch2, std::tuple<MyArch>>::value));
    // This is caught by a static assert
    // ASSERT_FALSE((IsCompatible<NotArch, std::tuple<MyArch, MyArch2>>::value));

}


} // namespace test
} // namespace panda
} // namespace ska
