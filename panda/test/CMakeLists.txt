include_directories(${GTEST_INCLUDE_DIR})
link_directories(${GTEST_LIBRARY_DIR})

set (TEST_UTIL_SRCS
  src/gtest.cpp
)
add_library (panda_testutils ${TEST_UTIL_SRCS})

set (TEST_SRCS
  src/ArchitectureTest.cpp
  src/ResourcePoolImplFactoryTest.cpp
)
add_executable(gtest_panda src/gtest_main.cpp ${TEST_SRCS})
target_link_libraries(gtest_panda panda_testutils panda_testutils ${PANDA_LIBRARIES} ${GTEST_LIBRARIES})

add_test (panda_unit_test gtest_panda)
